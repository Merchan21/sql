package facci.merchan.practicasql;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    ListView ListadeLibros;
    Button guardar, eliminar, modificar;
    EditText ingresar, editorial;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListadeLibros = findViewById(R.id.ListadeLibros);
        guardar = findViewById(R.id.crear);
        eliminar = findViewById(R.id.eliminar);
        modificar = findViewById(R.id.modificar);
        ingresar = findViewById(R.id.titulo);
        editorial = findViewById(R.id.Edicion);

        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DatosActivity.class);
                startActivity(intent);
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book registro1 = new Book(
                        ingresar.getText().toString(),
                        editorial.getText().toString());
                registro1.save();
            }
        });

        List<Book> books = Book.listAll((Book.class));
        List<String> listaTextoLibros = new ArrayList<>();

        for (Book libro: books){
            Log.e("Libro",
                    libro.getTitle()+""+libro.getEdition());
            listaTextoLibros.add(
                    libro.getTitle()+""+libro.getEdition());
        }
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                        listaTextoLibros);
        ListadeLibros.setAdapter(itemsAdapter);
    }
}
